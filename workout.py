from flask import request
from flask_restful import Resource, abort
from marshmallow import Schema, fields, validate
from marshmallow.exceptions import ValidationError
from sqlalchemy import Column, Integer, String, ForeignKey
from sqlalchemy.orm import relationship
from common import Base
from app import db
from exercise import Exercise, ExerciseSchema
from set import Set

class Workout(Base):
    __tablename__ = 'workouts'

    id = Column(Integer, primary_key=True)
    name = Column(String)
    desc = Column(String)
    exercises = relationship("Exercise", backref='workouts', cascade="all, delete-orphan")

    def __init__(self, name, desc, exercises):
        self.name = name
        self.desc = desc
        self.exercises = exercises

    def __repr__(self):
        return "<Workout '" + self.name + "'>"

    def to_json(self):
        """
        Returns the obj in a JSON format.
        """
        return {
            'id': self.id,
            'name': self.name,
            'desc': self.desc,
            'exercises': [ex.to_json() for ex in self.exercises]
        }


class WorkoutSchema(Schema):
    name = fields.String(require=True, allow_none=False)
    desc = fields.String(require=True, allow_none=False)
    exercises = fields.Nested(ExerciseSchema,
                              many=True,
                              allow_none=False,
                              required=True,
                              validate=validate.Length(
                                  min=1,
                                  error='Field may not be an empty list'))


class WorkoutAPI(Resource):
    #pylint: disable=R0201
    #pylint: disable=E1101

    def get(self, workout_id):
        try:
            if workout_id == 'all':
                return [wo.to_json() for wo in db.session.query(Workout).all()]

            wo = db.session.query(Workout).get(workout_id)
            return wo.to_json()
        except AttributeError:
            abort(400, message='Bad Request (ID does not exists)')
        except Exception:
            abort(500, message='Internal Server Error')


    def post(self):
        try:
            validated_json = WorkoutSchema().load(request.get_json())
            new_wo = Workout(validated_json['name'],
                             validated_json['desc'],
                             [Exercise(ex['name'],
                                       [Set(s['rep_nb'], s['weight'])
                                        for s in ex['sets']])
                              for ex in validated_json['exercises']]
                            )

            db.session.add(new_wo)
            db.session.commit()

            return new_wo.to_json()
        except ValidationError as err:
            abort(400, message=('Validation Error(s): ' + str(err)))
        except Exception:
            abort(500, message='Internal Server Error')


    def put(self, workout_id):
        try:
            validated_json = WorkoutSchema().load(request.get_json())
            wo = db.session.query(Workout).get(workout_id)
            if wo is not None:
                wo.name = validated_json['name']
                wo.desc = validated_json['desc']
                wo.exercises = [Exercise(ex['name'],
                                         [Set(s['rep_nb'], s['weight']) for s in ex['sets']]
                                        )
                                for ex in validated_json['exercises']]
                db.session.commit()

            return wo.to_json()
        except AttributeError:
            abort(400, message='Bad Request (ID does not exists)')
        except ValidationError as err:
            abort(400, message=('Validation Error(s): ' + str(err)))
        except Exception:
            abort(500, message='Internal Server Error')


    def delete(self, workout_id):
        try:
            wo = db.session.query(Workout).get(workout_id)
            if wo is not None:
                db.session.delete(wo)
                db.session.commit()
                return {'message': 'Object deleted'}

            abort(400, message='Bad Request (ID does not exists)')
        except Exception:
            abort(500, message='Internal Server Error')
