from flask import Flask
from flask_sqlalchemy import SQLAlchemy
from flask_restful import Api
from settings import SQLALCHEMY_DATABASE_URI, SQLALCHEMY_TRACK_MODIFICATIONS

APP = Flask(__name__)
APP.config['SQLALCHEMY_DATABASE_URI'] = SQLALCHEMY_DATABASE_URI
APP.config['SQLALCHEMY_TRACK_MODIFICATIONS'] = SQLALCHEMY_TRACK_MODIFICATIONS
db = SQLAlchemy(APP)

API = Api(APP)
