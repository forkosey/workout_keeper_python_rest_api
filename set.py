from flask import request
from flask_restful import Resource, abort
from marshmallow import Schema, fields
from marshmallow.exceptions import ValidationError
from sqlalchemy import Column, Integer, ForeignKey
from common import Base
from app import db

class Set(Base):
    __tablename__ = 'sets'

    id = Column(Integer, primary_key=True)
    rep_nb = Column(Integer)
    weight = Column(Integer)
    exercise_id = Column(Integer, ForeignKey('exercises.id'))

    def __init__(self, rep_nb=None, weight=None):
        self.rep_nb = rep_nb
        self.weight = weight


    def __repr__(self):
        return '<Set (' + str(self.rep_nb) + ', ' + str(self.weight) + ')>'


    def to_json(self):
        """
        Returns the obj in a JSON format.
        """
        return {
            'id': self.id,
            'rep_nb': str(self.rep_nb),
            'weight': str(self.weight)
        }


class SetSchema(Schema):
    rep_nb = fields.Integer(required=True, allow_none=False)
    weight = fields.Integer(required=True, allow_none=False)


class SetAPI(Resource):
    #pylint: disable=R0201
    #pylint: disable=E1101

    def get(self, set_id):
        try:
            if set_id == 'all':
                return [set_obj.to_json() for set_obj in db.session.query(Set).all()]

            set_obj = db.session.query(Set).get(set_id)
            return set_obj.to_json()
        except AttributeError:
            abort(400, message='Bad Request (ID does not exists)')
        except Exception:
            abort(500, message='Internal Server Error')


    def post(self):
        try:
            validated_json = SetSchema().load(request.get_json())
            new_set = Set(rep_nb=validated_json['rep_nb'], weight=validated_json['weight'])
            db.session.add(new_set)
            db.session.commit()
            return new_set.to_json()
        except ValidationError as err:
            abort(400, message=('Validation Error(s): ' + str(err)))
        except Exception:
            abort(500, message='Internal Server Error')


    def put(self, set_id):
        try:
            validated_json = SetSchema().load(request.get_json())
            set_obj = db.session.query(Set).get(set_id)
            if set_obj is not None:
                set_obj.rep_nb = validated_json['rep_nb']
                set_obj.weight = validated_json['weight']
                db.session.commit()

            return set_obj.to_json()
        except AttributeError:
            abort(400, message='Bad Request (ID does not exists)')
        except ValidationError as err:
            abort(400, message=('Validation Error(s): ' + str(err)))
        except Exception:
            abort(500, message='Internal Server Error')


    def delete(self, set_id):
        try:
            set_obj = db.session.query(Set).get(set_id)
            if set_obj is not None:
                db.session.delete(set_obj)
                db.session.commit()
                return {'message': 'Object deleted'}

            abort(400, message='Bad Request (ID does not exists)')
        except Exception:
            abort(500, message='Internal Server Error')
