from sqlalchemy.ext.declarative import declarative_base
from common import Base
from app import API, APP, db
from workout import WorkoutAPI
from exercise import ExerciseAPI
from set import SetAPI

api_root_routing = '/api/workout_keeper'

API.add_resource(SetAPI, api_root_routing + '/set', api_root_routing + '/set/<string:set_id>')
API.add_resource(ExerciseAPI, api_root_routing + '/exercise', api_root_routing + '/exercise/<string:exercise_id>')
API.add_resource(WorkoutAPI, api_root_routing + '/workout', api_root_routing + '/workout/<string:workout_id>')

def setup():
    Base.metadata.drop_all(bind=db.engine)
    Base.metadata.create_all(bind=db.engine)

setup()

if __name__ == '__main__':
    APP.run('0.0.0.0', debug=True)
