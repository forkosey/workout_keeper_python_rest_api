from flask import request
from flask_restful import Resource, abort
from marshmallow import Schema, fields, validate
from marshmallow.exceptions import ValidationError
from sqlalchemy import Column, Integer, String, ForeignKey
from sqlalchemy.orm import relationship
from common import Base
from app import db
from set import Set, SetSchema

class Exercise(Base):
    __tablename__ = 'exercises'

    id = Column(Integer, primary_key=True)
    name = Column(String)
    sets = relationship("Set", backref='exercises', cascade="all, delete-orphan")
    workout_id = Column(Integer, ForeignKey('workouts.id'))

    def __init__(self, name, sets):
        self.name = name
        self.sets = sets


    def __repr__(self):
        return "<Exercise '" + self.name + "'>"


    def to_json(self):
        """
        Returns the obj in a JSON format.
        """
        return {
            'id': self.id,
            'name': str(self.name),
            'sets': [set_obj.to_json() for set_obj in self.sets]
        }


class ExerciseSchema(Schema):
    name = fields.String(required=True, allow_none=False)
    sets = fields.Nested(SetSchema,
                         many=True,
                         allow_none=False,
                         required=True,
                         validate=validate.Length(
                             min=1,
                             error='Field may not be an empty list'))


class ExerciseAPI(Resource):
    #pylint: disable=R0201
    #pylint: disable=E1101

    def get(self, exercise_id):
        try:
            if exercise_id == 'all':
                return [ex.to_json() for ex in db.session.query(Exercise).all()]

            ex = db.session.query(Exercise).get(exercise_id)
            return ex.to_json()
        except AttributeError:
            abort(400, message='Bad Request (ID does not exists)')
        except Exception:
            abort(500, message='Internal Server Error')


    def post(self):
        try:
            validated_json = ExerciseSchema().load(request.get_json())
            new_exercise = Exercise(validated_json['name'],
                                    [Set(x['rep_nb'], x['weight']) for x in validated_json['sets']])

            db.session.add(new_exercise)
            db.session.commit()

            return new_exercise.to_json()
        except ValidationError as err:
            abort(400, message=('Validation Error(s): ' + str(err)))
        except Exception:
            abort(500, message='Internal Server Error')


    def put(self, exercise_id):
        try:
            validated_json = ExerciseSchema().load(request.get_json())
            ex = db.session.query(Exercise).get(exercise_id)
            if ex is not None:
                ex.name = validated_json['name']
                ex.sets = [Set(x['rep_nb'], x['weight']) for x in validated_json['sets']]
                db.session.commit()

            return ex.to_json()
        except AttributeError:
            abort(400, message='Bad Request (ID does not exists)')
        except ValidationError as err:
            abort(400, message=('Validation Error(s): ' + str(err)))
        except Exception:
            abort(500, message='Internal Server Error')


    def delete(self, exercise_id):
        try:
            ex = db.session.query(Exercise).get(exercise_id)
            if ex is not None:
                db.session.delete(ex)
                db.session.commit()
                return {'message': 'Object deleted'}

            abort(400, message='Bad Request (ID does not exists)')
        except Exception:
            abort(500, message='Internal Server Error')
