#!/bin/bash

YELLOW='\033[0;33m'
GREEN='\033[0;32m'
NC='\033[0m'

deactivate || true
echo -e "${YELLOW}Server restarting...${NC}"
pkill gunicorn
sudo systemctl daemon-reload
sudo systemctl start flask_server
sudo systemctl enable flask_server
sudo systemctl restart nginx
echo -e "${GREEN}Server is up and running again!${NC}"
